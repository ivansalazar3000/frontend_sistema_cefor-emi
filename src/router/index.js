import Vue from 'vue'
import Router from 'vue-router'
import LoginPage from '@/pages/Login/LoginPage.vue'
import UnidadAcademicaPage from '@/pages/UnidadAcademica/UnidadAcademicaPage.vue'
import RolPage from '@/pages/Rol/RolPage.vue'
import PersonaPage from '@/pages/Persona/PersonaPage.vue'
import PrincipalPage from '@/pages/Principal/PrincipalPage.vue'
import ProfilePage from '@/pages/Profile/ProfilePage.vue'
import ProfileCeforPage from '@/pages/ProfileCefor/ProfileCeforPage.vue'
import TipoReportePage from '@/pages/TipoReporte/TipoReportePage.vue'
import TipoReporteVerPage from '@/pages/TipoReporteVer/TipoReporteVerPage.vue'


import TipoCompaniaPage from '@/pages/TipoCompania/TipoCompaniaPage.vue'
import TipoDocumentoPage from '@/pages/TipoDocumento/TipoDocumentoPage.vue'
import TipoAsistenciaPage from '@/pages/TipoAsistencia/TipoAsistenciaPage.vue'
import EscuadraPage from '@/pages/Escuadra/EscuadraPage.vue'
import SeccionPage from '@/pages/Seccion/SeccionPage.vue'
import CompaniaPage from '@/pages/Compania/CompaniaPage.vue'
import EstadoAsistenciaPage from '@/pages/EstadoAsistencia/EstadoAsistenciaPage.vue'
import EstadoPersonaPage from '@/pages/EstadoPersona/EstadoPersonaPage.vue'
import EstadoDocumentoPage from '@/pages/EstadoDocumento/EstadoDocumentoPage.vue'

import CompaniaSeccionPage from '@/pages/CompaniaSeccion/CompaniaSeccionPage.vue'
import DocumentacionPage from '@/pages/Documentacion/DocumentacionPage.vue'
import AsistenciaPage from '@/pages/Asistencia/AsistenciaPage.vue'
import CertificacionPage from '@/pages/Certificacion/CertificacionPage.vue'


Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'PrincipalPage',
      component: PrincipalPage
    },
    {
      path: '/UnidadAcademica',
      name: 'Unidad Academica',
      component: UnidadAcademicaPage
    },
    {
      path: '/Rol',
      name: 'Rol',
      component: RolPage
    },
    {
      path: '/Persona',
      name: 'Persona',
      component: PersonaPage
    },
    {
      path: '/Profile',
      name: 'Perfil',
      component: ProfilePage
    },
    {
      path: '/ProfileCefor',
      name: 'PerfilCefor',
      component: ProfileCeforPage
    },
    {
      path: '/Login',
      name: 'Login',
      component: LoginPage
    },
    {
      path: '/TipoReporte',
      name: 'TipoReporte',
      component: TipoReportePage
    },
    {
      path: '/TipoReporteVer',
      name: 'TipoReporteVer',
      component: TipoReporteVerPage
    },
    {
      path: '/TipoCompania',
      name: 'TipoCompania',
      component: TipoCompaniaPage
    },
    {
      path: '/TipoDocumento',
      name: 'TipoDocumento',
      component: TipoDocumentoPage
    },
    {
      path: '/TipoAsistencia',
      name: 'TipoAsistencia',
      component: TipoAsistenciaPage
    },
    {
      path: '/Escuadra',
      name: 'Escuadra',
      component: EscuadraPage
    },
    {
      path: '/Seccion',
      name: 'Seccion',
      component: SeccionPage
    },
    {
      path: '/Compania',
      name: 'Compania',
      component: CompaniaPage
    },
    {
      path: '/EstadoAsistencia',
      name: 'EstadoAsistencia',
      component: EstadoAsistenciaPage
    },
    {
      path: '/EstadoPersona',
      name: 'EstadoPersona',
      component: EstadoPersonaPage
    },
    {
      path: '/EstadoDocumento',
      name: 'EstadoDocumento',
      component: EstadoDocumentoPage
    },
    {
      path: '/CompaniaSeccion',
      name: 'CompaniaSeccion',
      component: CompaniaSeccionPage
    },
    {
      path: '/Documentacion',
      name: 'Documentacion',
      component: DocumentacionPage
    },
    {
      path: '/Asistencia',
      name: 'Asistencia',
      component: AsistenciaPage
    },
    {
      path: '/Certificacion',
      name: 'Certificacion',
      component: CertificacionPage
    }

  ]
})