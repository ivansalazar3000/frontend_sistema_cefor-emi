import MainService from "@/services/MainService.js";
import dataTable from "@/components/Datatable";
import Loader from "@/components/Loader";
window.$ = window.jQuery = require("jquery");
   
export default {
    name: "CertificadoPage",
   
    data() {
        let ss = new MainService();
        return {
            msg: "CertificadoPage",
           
            ss: ss,
            ajax: {
                "url": ss.indexCertificado(),
                headers: ss.getToken(),
            },
            columns: [
                { data: 'id', name: 'id', orderable: false, searchable: false, visible: false },
                { data: 'DT_RowIndex', name: 'DT_RowIndex', title: 'N', orderable: false, searchable: false },
                { data: 'NombreEscuadra', name: 'e.NombreEscuadra', title: 'Escuadra' },
                { data: 'NombreSeccion', name: 's.NombreSeccion', title: 'Seccion' },
                { data: 'NombreCompania', name: 'c.NombreCompania', title: 'Compania' },
                { data: 'Gestion', name: 'c.Gestion', title: 'Gestión' },
                { data: 'Periodo', name: 'c.Periodo', title: 'Periodo de Instrucción' },
                {
                    data: 'action',
                    orderable: false,
                    title: 'Acciones',
                    searchable: false
                },
            ],

            tipoAsistencias: [],
            personasporcomsecesc: [],
            companiaSeccion: {}, 
            companiaSeccionEscuadra:{},
            asistencia: {},
            isLoading: false,
            errorBag: {},
            totalAsistencia: {},

            date:'',
            fecha:'',
            min: '2023',
            max: (new Date(Date.now() - (new Date()).getTimezoneOffset() * 60000)).toISOString().substr(0, 10),
            colorHabilitado: 'green',
            colorObservado: 'yelow',
            colorNoHabilitado: 'red'
        };
    },

    

    methods: {

        colorEstado: function(){

            var val = this.pcse.EstadoPersona;
            if(val == "Habilitado"){
                return 'badge badge-pill badge-success';
            }
            if(val == "En observacion"){
                return 'badge badge-pill badge-warning';
            }
            if(val == "No habilitado"){
                return 'badge badge-pill badge-danger';
            }
    
    
        },

       


        getTipoAsistencia() {
            this.ss.listTipoAsistencia().then(
                (result) => {
                    let response = result.data;
                    this.tipoAsistencias = response.data;
                }
            ).catch(error => {
                console.log(error);
            });
        },


        getPersonasporEscuadras(id) {                
            this.ecs = id;
            this.companiaSeccionEscuadra.id = this.ecs;                                   //parametro id
            this.ss.listCompSecEscPersona(this.companiaSeccionEscuadra.id).then(


                (result) => {

                    let response = result.data;
                    this.personasporcomsecesc = response.data;
                    console.log(response);
                }
            ).catch(error => {
                console.log(error);
            });
        },






        showCompaniaSeccionEscuadra(id) {

            this.asistencia = {};
            this.isLoading=true;
            this.ss. showCompaniaSeccionEscuadra(id).then(
                (result) => {
                    let response = result.data;
                    this.asistencia = response.data;
                    this.$refs['view-estudiantes'].show();
                    this.isLoading=false;

                    this.pcse = id
                    this.getPersonasporEscuadras(this.pcse)
                    this.getTipoAsistencia()

                }
            ).catch(error => {
                console.log(error);
                this.isLoading=false;
            });
        },


        //-------certificacion----------
        certificacionPDF(persona) {
            
            this.per=persona.id
            //console.log(this.per);
            
            this.ss.generarCertificado(this.per).then(
                result => {
                    console.log(result);
                    //this.isLoading = false;
                    let response = result.data;
                    if (response.success) {
                        //this.setReset();

                        this.$bvToast.toast(
                            response.msg,
                            {
                                title: 'Correcto',
                                variant: 'success',
                                autoHideDelay: 5000
                            }
                        )

                            
                        var a = document.createElement("a");
                        a.href = response.data.url;
                        a.target = '_blank';
                        a.click();

                        console.log(response);
                        
                    } else {
                        this.$bvToast.toast(
                            response.msg,
                            {
                                title: 'Error',
                                variant: 'danger',
                                autoHideDelay: 5000
                            }
                        )
                    }
                    this.errorBag = {};
                })
                .catch(error => {
                    this.isLoading = false;
                    console.log(error);
                    this.$bvToast.toast(
                        'Error al generar el Certificado',
                        {
                            title: 'Error',
                            variant: 'danger',
                            autoHideDelay: 5000
                        }
                    )
                    this.errorBag = error.data.errors;
                })
        },





        
        cancelCertificado() {
            if (this.asistencia.id) {
                this.$refs['view-estudiantes'].hide();
            }
            
        },



    
        
        draw() {
            window.$('.btn-datatable-Asistencia').on('click', (evt) => {

                const data = window.$(evt.target)[0].id;
                //this.getPersonasporEscuadras(data);
                //this.showAsistencia(data);
                  this.showCompaniaSeccionEscuadra(data);
            });
        }
    },
    components: {
        dataTable,
        Loader
    },
    mounted() {
        var persona = JSON.parse(localStorage.getItem('persona'));
        if (!persona) {
            this.$router.push('/Login');
        }

        this.date = this.printDate();
    }
};
