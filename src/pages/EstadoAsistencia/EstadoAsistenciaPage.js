import MainService from "@/services/MainService.js";
import dataTable from "@/components/Datatable";
import Loader from "@/components/Loader";
window.$ = window.jQuery = require("jquery");
   
export default {
    name: "EstadoAsistenciaPage",
    data() {
        let ss = new MainService();
        return {
            msg: "EstadoAsistenciaPage",
            ss: ss,
            ajax: {
                "url": ss.indexEstadoAsistencia(),
                headers: ss.getToken(),
            },
            columns: [
                { data: 'id', name: 'id', orderable: false, searchable: false, visible: false },
                { data: 'DT_RowIndex', name: 'DT_RowIndex', title: 'N', orderable: false, searchable: false },
                { data: 'EstadoAsistencia', name: 'EstadoAsistencia.EstadoAsistencia', title: 'Estado de Asistencia' },
                { data: 'Descripcion', name: 'EstadoAsistencia.Descripcion', title: 'Descripción' },
                {
                    data: 'action',
                    orderable: false,
                    title: 'Acciones',
                    searchable: false
                },
            ],
            estadoAsistencia: {},
            isLoading: false,
            errorBag: {}
        };
    },
    methods: {
        newEstadoAsistencia() {
            this.estadoAsistencia = {};
            this.$refs['frm-estadoAsistencia'].show();
        },
        showEstadoAsistencia(id) {
            this.isLoading=true;
            this.ss.showEstadoAsistencia(id).then(
                (result) => {
                    let response = result.data;
                    this.estadoAsistencia = response.data;
                    this.$refs['view-estadoAsistencia'].show();
                    this.isLoading=false;
                }
            ).catch(error => {
                console.log(error);
                this.isLoading=false;
            });
        },
        editEstadoAsistencia() {
            this.$refs['frm-estadoAsistencia'].show();
            this.$refs['view-estadoAsistencia'].hide();
        },
        cancelEstadoAsistencia() {
            if (this.estadoAsistencia.id) {
                this.$refs['view-estadoAsistencia'].show();
            }
            this.$refs['frm-estadoAsistencia'].hide();
        },
        saveEstadoAsistencia() {
            this.ss.storeEstadoAsistencia(this.estadoAsistencia).then(
                (result) => {
                    console.log(result);
                    let response = result.data;
                    this.$bvToast.toast(
                        response.msg,
                        {
                            title: 'Correcto',
                            variant: 'success',
                            autoHideDelay: 5000
                        }
                    )
                    //this.$refs['view-consulta'].show(); //para volver al modal
                    console.log(response);
                    this.$refs['frm-estadoAsistencia'].hide();
                    this.$refs['datatable-estadoAsistencia'].reload();
                })
                .catch((error) => {
                    this.errorBag = error.response.data.errors;
                    this.$bvToast.toast(
                        'Problema al Guardar el Registro, favor verificar los Datos',
                        {
                            title: 'Error',
                            variant: 'danger',
                            autoHideDelay: 5000
                        }
                    )
                });
        },
        deleteEstadoAsistencia() {
            this.$swal({
                title: "Estas seguro que deseas eliminar?",
                text: "Esta accion es irreversible!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {
                        this.ss.destroyEstadoAsistencia(this.estadoAsistencia)
                            .then((result) => {
                                let response = result.data;
                                console.log(response);
                                this.$bvToast.toast(
                                    response.msg,
                                    {
                                        title: 'Correcto',
                                        variant: 'success',
                                        autoHideDelay: 5000
                                    }
                                )
                                this.$refs['view-estadoAsistencia'].hide();
                                this.$refs['datatable-estadoAsistencia'].reload();
                            })
                            .catch(error => {
                                console.log(error);
                            })
                    } else {
                        //swal("Your imaginary file is safe!");
                    }
                });
        },
        draw() {
            window.$('.btn-datatable-EstadoAsistencia').on('click', (evt) => {
                const data = window.$(evt.target)[0].id;
                this.showEstadoAsistencia(data);
            });
        }
    },
    components: {
        dataTable,
        Loader
    },
    mounted() {
        var persona = JSON.parse(localStorage.getItem('persona'));
        if (!persona) {
            this.$router.push('/Login');
        }
    }
};
