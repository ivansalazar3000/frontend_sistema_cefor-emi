import MainService from "@/services/MainService.js";
import dataTable from "@/components/Datatable";
import Loader from "@/components/Loader";
window.$ = window.jQuery = require("jquery");

export default {
    name: "CompaniaPage",
    data() {
        let ss = new MainService();
        return {
            msg: "CompaniaPage",
            ss: ss,
            ajax: {
                "url": ss.indexCompania(),
                headers: ss.getToken(),
            },
            columns: [
                { data: 'id', name: 'id', orderable: false, searchable: false, visible: false },
                { data: 'DT_RowIndex', name: 'DT_RowIndex', title: 'N', orderable: false, searchable: false },
                { data: 'NombreCompania', name: 'c.NombreCompania', title: 'Nombre de Compañia' },
                { data: 'TipoCompania', name: 'tc.TipoCompania', title: 'Tipo de Compañia' },
                { data: 'Persona', name: 'p.Persona', title: 'Comandante de Compañia' },
                { data: 'Gestion', name: 'c.Gestion', title: 'Gestión' },
                { data: 'Periodo', name: 'c.Periodo', title: 'Periodo' },
                {
                    data: 'action',
                    orderable: false,
                    title: 'Acciones',
                    searchable: false
                },
            ],
            tipoCompanias: [],//anida datos del list
            personas: [],
            tipoCompania: {},
            compania: {},//nuevo objetito
            isLoading: false,
            errorBag: {},
            year:'',
            d:''

            
        };
        
        
    },
    methods: {

        onChange:function(event){
            console.log(event);
            this.ss.showTipoCompania(event).then(
                (result) => {
                    console.log(result);

                    var parametros = result.data.data.Descripcion;
                    console.log(parametros);
                    this.d=parametros;

                }    
            ); 

        },

        printYear: function () {
            return new Date().getFullYear();
            
          },


        getTipoCompania() {
            this.ss.listTipoCompania().then(
                (result) => {
                    let response = result.data;
                    this.tipoCompanias = response.data;
                }
            ).catch(error => {
                console.log(error);
            });
        },


        getPersonaColaborador() {
            this.ss.listPersonaColaborador().then(
                (result) => {
                    let response = result.data;
                    this.personas = response.data;
                }
            ).catch(error => {
                console.log(error);
            });
        },

        
        getMyFunctionYear: function(){

            let fecha = new Date();
            let ano = fecha.getFullYear();
            this.$refs.gestion.value = ano;
            
        },

        newCompania() {
            this.compania = {};
            this.$refs['frm-compania'].show();
        },
        showCompania(id) {
           
            this.isLoading=true;
            this.ss.showCompania(id).then(
                (result) => {
                    let response = result.data;
                    this.compania = response.data;
                    this.$refs['view-compania'].show();
                    
                    this.isLoading=false;
                }
            ).catch(error => {
                console.log(error);
                this.isLoading=false;
            });
        },

        editCompania() {
            this.$refs['frm-compania'].show();
            this.$refs['view-compania'].hide();
        },
        cancelCompania() {
            if (this.compania.id) {
                this.$refs['view-compania'].show();
            }
            this.$refs['frm-compania'].hide();
        },
        saveCompania() {
            this.ss.storeCompania(this.compania).then(
                (result) => {
                    console.log(result);
                    let response = result.data;
                    this.$bvToast.toast(
                        response.msg,
                        {
                            title: 'Correcto',
                            variant: 'success',
                            autoHideDelay: 5000
                        }
                    )
                    
                    console.log(response);
                    this.$refs['frm-compania'].hide();
                    this.$refs['datatable-compania'].reload();
                })
                .catch((error) => {
                    this.errorBag = error.response.data.errors;
                    this.$bvToast.toast(
                        'Problema al Guardar el Registro, favor verificar los Datos',
                        {
                            title: 'Error',
                            variant: 'danger',
                            autoHideDelay: 5000
                        }
                    )
                });
        },
        deleteCompania() {
            this.$swal({
                title: "Estas seguro que deseas eliminar?",
                text: "Esta accion es irreversible!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {
                        this.ss.destroyCompania(this.compania)
                            .then((result) => {
                                let response = result.data;
                                console.log(response);
                                this.$bvToast.toast(
                                    response.msg,
                                    {
                                        title: 'Correcto',
                                        variant: 'success',
                                        autoHideDelay: 5000
                                    }
                                )
                                this.$refs['view-compania'].hide();
                                this.$refs['datatable-compania'].reload();
                            })
                            .catch(error => {
                                console.log(error);
                            })
                    } else {
                        //swal("Your imaginary file is safe!");
                    }
                });
        },
        draw() {
            window.$('.btn-datatable-Compania').on('click', (evt) => {
                const data = window.$(evt.target)[0].id;
                this.showCompania(data);
                
            });
        }
    },
    components: {
        dataTable,
        Loader
    },
    mounted() {
        var persona = JSON.parse(localStorage.getItem('persona'));
        if (!persona) {
            this.$router.push('/Login');
        } else {
            this.getTipoCompania();
            this.getPersonaColaborador();
            
            
        }

        this.year = this.printYear();
        
    }
};
