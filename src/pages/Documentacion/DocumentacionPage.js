import MainService from "@/services/MainService.js";
import dataTable from "@/components/Datatable";
import WebViewer from "@/components/WebViewer";
import Loader from "@/components/Loader";
window.$ = window.jQuery = require("jquery");

export default {
    name: "DocumentacionPage",
    data() {
        let ss = new MainService();
        return {
            
            msg: "DocumentacionPage",
            ss: ss,
            ajax: {
                "url": ss.indexDocumento(),
                headers: ss.getToken(),
            },
            columns: [
                { data: 'id', name: 'id', orderable: false, searchable: false, visible: false },
                { data: 'DT_RowIndex', name: 'DT_RowIndex', title: 'N', orderable: false, searchable: false },
                { data: 'Persona', name: 'p.Persona', title: 'Estudiante' },
                //{ data: 'Documento', name: 'Documento', title: 'Documento' },
                { data: 'TipoDocumento', name: 'td.TipoDocumento', title: 'Tipo de Documento' },
                { data: 'EstadoDocumento', name: 'ed.EstadoDocumento', title: 'Estado' },
                { data: 'FechaEntrega', name: 'd.FechaEntrega', title: 'Fecha de Entrega' },
                
                {
                    data: 'action',
                    orderable: false,
                    title: 'Acciones',
                    searchable: false
                },
            ],

            auth:{},
            documento:{},
            estadoDocumentos:[],
            tipoDocumentos:[],
            isLoading: false,
            isLoadingFile: false,
            errorBag: {},
            ruta: 0,
            date:'',
            max: (new Date(Date.now() - (new Date()).getTimezoneOffset() * 60000)).toISOString().substr(0, 10)
            
        };
        
       
    },
    methods: {

        loadFile(input) {
            this.isLoadingFile = true;
            input = event.target;
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.readAsDataURL(input.files[0]);
                var data = new FormData();
                data.append('File', input.files[0]);
                this.ss.uploadFile(data)
                    .then(result => {
                        if (result.data.success) {
                            this.$bvToast.toast(
                                result.data.msg,
                                {
                                    title: 'Correcto',
                                    variant: 'info',
                                    autoHideDelay: 5000
                                }
                            )
                            this.documento.Documento = result.data.data;
                            this.ruta = "http://localhost:8000/storage/documents/documents/" + this.documento.Documento;
                        } else {
                            this.$bvToast.toast(
                                result.data.msg,
                                {
                                    title: 'Oops!',
                                    variant: 'danger',
                                    autoHideDelay: 5000
                                }
                            )
                        }
                        this.isLoadingFile = false;
                    })
                    .catch(error => {
                        console.log(error);
                        this.$bvToast.toast(
                            'Error subiendo archivo',
                            {
                                title: 'Oops!',
                                variant: 'danger',
                                autoHideDelay: 5000
                            }
                        )
                        this.isLoadingFile = false;
                    });
            }
        },

        
        getTipoDocumento() {
            this.ss.listTipoDocumento().then(
                (result) => {
                    let response = result.data;
                    this.tipoDocumentos = response.data;
                }
            ).catch(error => {
                console.log(error);
            });
        },


        getEstadoDocumento() {
            this.ss.listEstadoDocumento().then(
                (result) => {
                    let response = result.data;
                    this.estadoDocumentos = response.data;
                }
            ).catch(error => {
                console.log(error);
            });
        },

        
        myFunctionYear: function(){

            let fecha = new Date();
            let ano = fecha.getFullYear();
            this.$refs.fecha.value = ano;
            
        },

        printDate: function () {
            return new Date().toLocaleDateString();
          },


        newDocumento() {
            this.documento = {};
            this.$refs['frm-documento'].show();
            
        },

        showPersona() {
            this.ss.showPersona(this.auth.id)
                .then(result => {
                    let response = result.data;
                    this.persona = response.data;
                })
                .catch(error => {
                    console.log(error);
                });
        },

        
        listDocumentosporPersona(){
            this.ss.listDocumentosporPersona(this.auth.id)
                .then(result => {
                    let response = result.data;
                    this.persona = response.data;
                })
                .catch(error => {
                    console.log(error);
                });


        },

        showDocumento(id) {
           
            this.isLoading=true;
            this.ss.showDocumento(id).then(
                (result) => {
                    let response = result.data;
                    this.documento = response.data;
                    this.$refs['view-documento'].show();
                    
                    this.isLoading=false;
                }
            ).catch(error => {
                console.log(error);
                this.isLoading=false;
            });
        },

        editDocumento() {
            this.$refs['frm-documento'].show();
            this.$refs['view-documento'].hide();
        },
        cancelDocumento() {
            if (this.documento.id) {
                this.$refs['view-documento'].show();
            }
            this.$refs['frm-documento'].hide();
        },

        
        saveDocumento() {
            this.documento.Persona=this.auth.id;
            this.documento.EstadoDocumento = 1;
            this.documento.FechaEntrega = this.date;
            this.ss.storeDocumento(this.documento).then(
                (result) => {
                    console.log(result);
                    let response = result.data;
                    this.$bvToast.toast(
                        response.msg,
                        {
                            title: 'Correcto',
                            variant: 'success',
                            autoHideDelay: 5000
                        }
                    )
                    //this.$refs['view-consulta'].show(); //para volver al modal
                    console.log(response);
                    this.$refs['frm-documento'].hide();
                    this.$refs['datatable-documento'].reload();
                })
                .catch((error) => {
                    this.errorBag = error.response.data.errors;
                    this.$bvToast.toast(
                        'Problema al Guardar el Registro, favor verificar los Datos',
                        {
                            title: 'Error',
                            variant: 'danger',
                            autoHideDelay: 5000
                        }
                    )
                });
        },


        revisionDocumento(){
            this.ss.revisionDocumento(this.documento).then(
                (result) => {
                    console.log(result);
                    let response = result.data;
                    this.$bvToast.toast(
                        response.msg,
                        {
                            title: 'Correcto',
                            variant: 'success',
                            autoHideDelay: 5000
                        }
                    )
                    console.log(response);
                    this.$refs['frm-documento'].hide();
                    this.$refs['datatable-documento'].reload();
                })
                .catch((error) => {
                    this.errorBag = error.response.data.errors;
                    this.$bvToast.toast(
                        'Problema al Guardar el Registro, favor verificar los Datos',
                        {
                            title: 'Error',
                            variant: 'danger',
                            autoHideDelay: 5000
                        }
                    )
                });
        },



        deleteDocumento() {
            this.$swal({
                title: "Estas seguro que deseas eliminar?",
                text: "Esta accion es irreversible!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {
                        this.ss.destroyDocumento(this.documento)
                            .then((result) => {
                                let response = result.data;
                                console.log(response);
                                this.$bvToast.toast(
                                    response.msg,
                                    {
                                        title: 'Correcto',
                                        variant: 'success',
                                        autoHideDelay: 5000
                                    }
                                )
                                this.$refs['view-documento'].hide();
                                this.$refs['datatable-documento'].reload();
                            })
                            .catch(error => {
                                console.log(error);
                            })
                    } else {
                        //swal("Your imaginary file is safe!");
                    }
                });
        },
        draw() {
            window.$('.btn-datatable-Documentacion').on('click', (evt) => {
                const data = window.$(evt.target)[0].id;
                this.showDocumento(data);
            });
        }
    },
    components: {
        dataTable,
        WebViewer,
        Loader
    },
    

    mounted() {
        this.auth = JSON.parse(localStorage.getItem('persona'));
        console.log(this.auth);
        if (!this.auth) {
            this.$router.push('/Login');
        } else {
            this.showPersona();
            
            this.listDocumentosporPersona();

            this.getTipoDocumento();
            this.getEstadoDocumento();
            this.date = this.printDate();
            console.log(this.date);
        }
    }




};
