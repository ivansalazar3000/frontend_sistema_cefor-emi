import MainService from "@/services/MainService.js";
import dataTable from "@/components/Datatable";
import Loader from "@/components/Loader";
window.$ = window.jQuery = require("jquery");
   
export default {
    name: "AsistenciaPage",
   
    data() {
        let ss = new MainService();
        return {
            msg: "AsistenciaPage",
           
            ss: ss,
            ajax: {
                "url": ss.indexAsistencia(),
                headers: ss.getToken(),
            },
            columns: [
                { data: 'id', name: 'id', orderable: false, searchable: false, visible: false },
                { data: 'DT_RowIndex', name: 'DT_RowIndex', title: 'N', orderable: false, searchable: false },
                { data: 'NombreEscuadra', name: 'e.NombreEscuadra', title: 'Escuadra' },
                { data: 'NombreSeccion', name: 's.NombreSeccion', title: 'Sección' },
                { data: 'NombreCompania', name: 'c.NombreCompania', title: 'Compañia' },
                { data: 'Gestion', name: 'c.Gestion', title: 'Gestión' },
                { data: 'Periodo', name: 'c.Periodo', title: 'Periodo de Instrucción' },
                {
                    data: 'action',
                    orderable: false,
                    title: 'Acciones',
                    searchable: false
                },
            ],

            tipoAsistencias: [],
            personasporcomsecesc: [],
            companiaSeccion: {}, 
            companiaSeccionEscuadra:{},
            asistencia: {},
            isLoading: false,
            errorBag: {},
            totalAsistencia: {},

            date:'',
            fecha:'',
            min: '2023',
            max: (new Date(Date.now() - (new Date()).getTimezoneOffset() * 60000)).toISOString().substr(0, 10),
            colorHabilitado: 'green',
            colorObservado: 'yelow',
            colorNoHabilitado: 'red'
        };
    },

    computed:{

        

    },


    methods: {

        colorEstado: function(){

            var val = this.pcse.EstadoPersona;
            if(val == "Habilitado"){
                return 'badge badge-pill badge-success';
            }
            if(val == "En observacion"){
                return 'badge badge-pill badge-warning';
            }
            if(val == "No habilitado"){
                return 'badge badge-pill badge-danger';
            }
    
    
        },

       
        printDate: function () {
            return new Date().toLocaleDateString();
          },


        getTipoAsistencia() {
            this.ss.listTipoAsistencia().then(
                (result) => {
                    let response = result.data;
                    this.tipoAsistencias = response.data;
                }
            ).catch(error => {
                console.log(error);
            });
        },


        getPersonasporEscuadras(id) {                
            this.ecs = id;
            this.companiaSeccionEscuadra.id = this.ecs;                                   //parametro id
            this.ss.listCompSecEscPersona(this.companiaSeccionEscuadra.id).then(


                (result) => {

                    let response = result.data;
                    this.personasporcomsecesc = response.data;
                    console.log(response);
                }
            ).catch(error => {
                console.log(error);
            });
        },





        newAsistencia() {
            this.asistencia = {};
            this.$refs['frm-asistencia'].show();
        },


        showCompaniaSeccionEscuadra(id) {

            this.asistencia = {};
            this.isLoading=true;
            this.ss. showCompaniaSeccionEscuadra(id).then(
                (result) => {
                    let response = result.data;
                    this.asistencia = response.data;
                    this.$refs['view-estudiantes'].show();
                    this.isLoading=false;

                    this.pcse = id
                    this.getPersonasporEscuadras(this.pcse)
                    this.getTipoAsistencia()

                }
            ).catch(error => {
                console.log(error);
                this.isLoading=false;
            });
        },



        showAsistencia(id) {
            this.isLoading=true;
            this.ss.showAsistencia(id).then(
                (result) => {
                    let response = result.data;
                    this.asistencia = response.data;
                    this.$refs['view-asistencia'].show();
                    this.isLoading=false;
                }
            ).catch(error => {
                console.log(error);
                this.isLoading=false;
            });
        },



        tomarAsistencia() {
            this.$refs['frm-asistencia'].show();
            this.$refs['view-estudiantes'].hide();
        },


        tomarRevista() {
            this.$refs['frm-revista'].show();
            this.$refs['view-estudiantes'].hide();
        },


        cancelAsistencia() {
            if (this.asistencia.id) {
                this.$refs['view-asistencia'].show();
            }
            this.$refs['frm-asistencia'].hide();
        },

        cancelRevista() {
            if (this.asistencia.id) {
                this.$refs['view-asistencia'].show();
            }
            this.$refs['frm-revista'].hide();
        },





        saveAsistencia() {
            //this.asistencia = {};
           // this.asistencia.FechaAsistencia = "2023-04-14"
            //this.totalAsistencia.FechaAsistencia =  this.fecha;

            this.totalAsistencia.persona=this.personasporcomsecesc;

            this.ss.storeAsistencia(this.totalAsistencia).then(
                (result) => {
                    console.log(result);
                    let response = result.data;
                    this.$bvToast.toast(
                        response.msg,
                        {
                            title: 'Correcto',
                            variant: 'success',
                            autoHideDelay: 5000
                        }
                    )
                    //this.$refs['view-consulta'].show(); //para volver al modal
                    console.log(response);
                    this.$refs['frm-asistencia'].hide();
                    this.$refs['datatable-asistencia'].reload();
                })
                .catch((error) => {
                    this.errorBag = error.response.data.errors;
                    this.$bvToast.toast(
                        'Problema al Guardar el Registro, favor verificar los Datos',
                        {
                            title: 'Error',
                            variant: 'danger',
                            autoHideDelay: 5000
                        }
                    )
                });
        },


        saveRevista() {
            
            //this.personasporcomsecesc.RevistaMilitar = 1;
            
            this.totalAsistencia.persona=this.personasporcomsecesc;
            
            
            this.ss.storeRevista(this.totalAsistencia).then(
                (result) => {
                    console.log(result);
                    let response = result.data;
                    this.$bvToast.toast(
                        response.msg,
                        {
                            title: 'Correcto',
                            variant: 'success',
                            autoHideDelay: 5000
                        }
                    )
                    //this.$refs['view-consulta'].show(); //para volver al modal
                    console.log(response);
                    this.$refs['frm-revista'].hide();
                    this.$refs['datatable-asistencia'].reload();
                })
                .catch((error) => {
                    this.errorBag = error.response.data.errors;
                    this.$bvToast.toast(
                        'Problema al Guardar el Registro, favor verificar los Datos',
                        {
                            title: 'Error',
                            variant: 'danger',
                            autoHideDelay: 5000
                        }
                    )
                });
        },







        deleteAsistencia() {
            this.$swal({
                title: "Estas seguro que deseas eliminar?",
                text: "Esta accion es irreversible!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {
                        this.ss.destroyAsistencia(this.asistencia)
                            .then((result) => {
                                let response = result.data;
                                console.log(response);
                                this.$bvToast.toast(
                                    response.msg,
                                    {
                                        title: 'Correcto',
                                        variant: 'success',
                                        autoHideDelay: 5000
                                    }
                                )
                                this.$refs['view-asistencia'].hide();
                                this.$refs['datatable-asistencia'].reload();
                            })
                            .catch(error => {
                                console.log(error);
                            })
                    } else {
                        //swal("Your imaginary file is safe!");
                    }
                });
        },
        draw() {
            window.$('.btn-datatable-Asistencia').on('click', (evt) => {

                const data = window.$(evt.target)[0].id;
                //this.getPersonasporEscuadras(data);
                //this.showAsistencia(data);
                  this.showCompaniaSeccionEscuadra(data);
            });
        }
    },
    components: {
        dataTable,
        Loader
    },
    mounted() {
        var persona = JSON.parse(localStorage.getItem('persona'));
        if (!persona) {
            this.$router.push('/Login');
        }

        this.date = this.printDate();
    }
};
