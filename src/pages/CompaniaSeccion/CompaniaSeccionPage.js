import MainService from "@/services/MainService.js";
import dataTable from "@/components/Datatable";
import Loader from "@/components/Loader";
window.$ = window.jQuery = require("jquery");

export default {
    
    name: "CompaniaSeccionPage",
    data() {

        let ss = new MainService();
        return {
            
            msg: "CompaniaSeccionPage",
            ss: ss,
            ajax: {
                "url": ss.indexCompaniaSeccion(),
                headers: ss.getToken(),
            },
            columns: [
                { data: 'id', name: 'id', orderable: false, searchable: false, visible: false },
                { data: 'DT_RowIndex', name: 'DT_RowIndex', title: 'N', orderable: false, searchable: false },
                { data: 'NombreCompania', name: 'c.NombreCompania', title: 'Nombre de Compañia' },
                { data: 'NombreSeccion', name: 's.NombreSeccion', title: 'Nombre de Sección' },
                { data: 'Descripcion', name: 's.Descripcion', title: 'Descripción de la Sección' },
                
                {
                    data: 'action',
                    orderable: false,
                    title: 'Acciones',
                    searchable: false
                },
            ],
            secciones: [],
            companias: [],
            escuadras: [],
            personas: [],
            escuadrasporcomsec: [],
            companiaSecciones: [],
            personasporcomsecesc: [],
            estadoPersonas: [],


            companiaSeccion: {}, 
            companiaSeccionEscuadra:{},
            compSecEscPersona:{},
            persona:{},
            isLoading: false,
            errorBag: {},
            ecs:0,
            cse:0,
            d:"",
            g:"",
            p:"",
            e:""

            
        };
    },
    methods: {

        onChange:function(event){
            console.log(event);
            this.ss.showSeccion(event).then(
                (result) => {
                    console.log(result);

                    var parametros = result.data.data.Descripcion;
                    console.log(parametros);
                    this.d=parametros;

                }    
            ); 

        },


        onChangeCompania:function(event){
            console.log(event);
            this.ss.showCompania(event).then(
                (result) => {
                    console.log(result);

                    var gestion = result.data.data.Gestion;
                    var periodo = result.data.data.Periodo;
                    console.log(gestion);
                    this.g=gestion;
                    this.p=periodo;

                }    
            ); 

        },

        onChangeEscuadra:function(event){
            console.log(event);
            this.ss.showEscuadra(event).then(
                (result) => {
                    console.log(result);

                    var parametros = result.data.data.Descripcion;
                    console.log(parametros);
                    this.e=parametros;

                }    
            ); 

        },


        getCompania() {
            this.ss.listCompania().then(
                (result) => {
                    let response = result.data;
                    this.companias = response.data;
                }
            ).catch(error => {
                console.log(error);
            });
        },

        getSeccion() {
            this.ss.listSeccion().then(
                (result) => {
                    let response = result.data;
                    this.secciones = response.data;
                }
            ).catch(error => {
                console.log(error);
            });
        },

        getEscuadra() {
            this.ss.listEscuadra().then(
                (result) => {
                    let response = result.data;
                    this.escuadras = response.data;
                }
            ).catch(error => {
                console.log(error);
            });
        },

        getPersona() {
            this.ss.listPersona().then(
                (result) => {
                    let response = result.data;
                    this.personas = response.data;
                }
            ).catch(error => {
                console.log(error);
            });
        },

        getEstadoPersona() {
            this.ss.listEstadoPersona().then(
                (result) => {
                    let response = result.data;
                    this.estadoPersonas = response.data;
                }
            ).catch(error => {
                console.log(error);
            });
        },

        
        getEscuadraporComSec() {                //parametro id
            this.ss.listCompaniaSeccionEscuadra(this.companiaSeccion.id).then(
                (result) => {
                    let response = result.data;
                    this.escuadrasporcomsec = response.data;
                }
            ).catch(error => {
                console.log(error);
            });
        },


        getPersonasporEscuadras(id) {                
            this.ecs = id;
            this.companiaSeccionEscuadra.id = this.ecs;                                   //parametro id
            this.ss.listCompSecEscPersona(this.companiaSeccionEscuadra.id).then(
                (result) => {

                    let response = result.data;
                    this.personasporcomsecesc = response.data;
                    console.log(response);
                }
            ).catch(error => {
                console.log(error);
            });
        },


        getListaPersonasporEscuadras() {                
                                             //parametro id
            this.ss.listCompSecEscPersona(this.companiaSeccionEscuadra.id).then(
                (result) => {

                    let response = result.data;
                    this.personasporcomsecesc = response.data;
                    console.log(response);
                }
            ).catch(error => {
                console.log(error);
            });
        },

        /******************************************************************************************/
        
        newCompaniaSeccion() {
            this.companiaSeccion = {};
            this.$refs['frm-companiaSeccion'].show();
        },


        showCompaniaSeccion(id) {

            this.isLoading=true;
            this.ss.showCompaniaSeccion(id).then(
                (result) => {
                    let response = result.data;
                    this.companiaSeccion = response.data;
                    console.log(this.companiaSeccion);
                    this.$refs['view-companiaSeccion'].show();
                    this.isLoading=false;
                    
                    this.getEscuadraporComSec();
                    this.getListaPersonasporEscuadras();
                    this.getEstadoPersona();
                    //this.getPersonasporEscuadras();
                }
            ).catch(error => {
                console.log(error);
                this.isLoading=false;
            });
        },


        editCompaniaSeccion() {
            this.$refs['frm-companiaSeccion'].show();
            this.$refs['view-companiaSeccion'].hide();
        },


        cancelCompaniaSeccion() {
            if (this.companiaSeccion.id) {
                this.$refs['view-companiaSeccion'].show();
            }
            this.$refs['frm-companiaSeccion'].hide();
        },

        saveCompaniaSeccion() {
            
            this.ss.storeCompaniaSeccion(this.companiaSeccion).then(
                (result) => {
                    console.log(result);
                    let response = result.data;
                    this.$bvToast.toast(
                        response.msg,
                        {
                            title: 'Correcto',
                            variant: 'success',
                            autoHideDelay: 5000
                        }
                    )
                    //this.$refs['view-consulta'].show(); //para volver al modal
                    console.log(response);
                    this.$refs['frm-companiaSeccion'].hide();
                    this.$refs['datatable-companiaSeccion'].reload();
                })
                .catch((error) => {
                    this.errorBag = error.response.data.errors;
                    this.$bvToast.toast(
                        'Problema al Guardar el Registro, favor verificar los Datos',
                        {
                            title: 'Error',
                            variant: 'danger',
                            autoHideDelay: 5000
                        }
                    )
                    
                });
        },

        deleteCompaniaSeccion() {
            this.$swal({
                title: "Estas seguro que deseas eliminar?",
                text: "Esta accion es irreversible!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {
                        this.ss.destroyCompaniaSeccion(this.companiaSeccion)
                            .then((result) => {
                                let response = result.data;
                                console.log(response);
                                this.$bvToast.toast(
                                    response.msg,
                                    {
                                        title: 'Correcto',
                                        variant: 'success',
                                        autoHideDelay: 5000
                                    }
                                )
                                this.$refs['view-companiaSeccion'].hide();
                                this.$refs['datatable-companiaSeccion'].reload();
                            })
                            .catch(error => {
                                console.log(error);
                            })
                    } else {
                        //swal("Your imaginary file is safe!");
                    }
                });
        },

        /******************************************************************************************/

        saveCmSecEsc(){
            
            this.companiaSeccionEscuadra.CompaniaSeccion = this.companiaSeccion.id;
              this.ss.storeCompaniaSeccionEscuadra(this.companiaSeccionEscuadra).then(
                (result) => {
                    console.log(result);
                    let response = result.data;
                    this.$bvToast.toast(
                        response.msg,
                        {
                            title: 'Correcto',
                            variant: 'success',
                            autoHideDelay: 5000
                        }
                    )
                    //this.$refs['view-consulta'].show(); //para volver al modal
                    console.log(response);
                    this.getEscuadraporComSec();
                   
                })
                .catch((error) => {
                    this.errorBag = error.response.data.errors;
                    this.$bvToast.toast(
                        'Problema al Guardar el Registro, favor verificar los Datos',
                        {
                            title: 'Error',
                            variant: 'danger',
                            autoHideDelay: 5000
                        }
                    )
                });
        },

        deleteCmSecEsc(id) {
            
            this.$swal({
                title: "Estas seguro que deseas eliminar la Escuadra de la Sección?",
                text: "Esta accion es irreversible!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {
                        this.ecs=id;
                        this.companiaSeccionEscuadra.id = this.ecs;
                        this.ss.destroyCompaniaSeccionEscuadra(this.companiaSeccionEscuadra)
                            .then((result) => {
                                let response = result.data;
                                console.log(response);
                                this.$bvToast.toast(
                                    response.msg,
                                    {
                                        title: 'Correcto',
                                        variant: 'success',
                                        autoHideDelay: 5000
                                    }
                                )
      
                                this.getEscuadraporComSec();
                            })
                            .catch(error => {
                                console.log(error);
                            })
                    } else {
                        //swal("Your imaginary file is safe!");
                    }
                });
        },

        /******************************************************************************************/



        saveCompSecEscPersona(){
            
            this.compSecEscPersona.CompaniaSeccionEscuadra = this.companiaSeccionEscuadra.id;
            this.compSecEscPersona.EstadoPersona = 1;
            this.compSecEscPersona.RevistaMilitar = 0;
            console.log(this.compSecEscPersona);
            this.ss.storeCompSecEscPersona(this.compSecEscPersona).then(
              (result) => {
                  console.log(result);
                  let response = result.data;
                  this.$bvToast.toast(
                      response.msg,
                      {
                          title: 'Correcto',
                          variant: 'success',
                          autoHideDelay: 5000
                      }
                  )
                  //this.$refs['view-consulta'].show(); //para volver al modal
                  //console.log(response);
                  this.getListaPersonasporEscuadras();
                  //this.getPersonasporEscuadras();
                 
              })
              .catch((error) => {
                  this.errorBag = error.response.data.errors;
                  this.$bvToast.toast(
                      'Problema al Guardar el Registro, favor verificar los Datos',
                      {
                          title: 'Error',
                          variant: 'danger',
                          autoHideDelay: 5000
                      }
                  )
              });
        },

        deleteCmSecEscPer(id) {
            
            this.$swal({
                title: "Estas seguro que deseas eliminar el estudiante de la Escuadra?",
                text: "Esta accion es irreversible!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {
                        this.ecsp=id;
                        this.compSecEscPersona.id = this.ecsp;
                        this.ss.destroyCompSecEscPersona(this.compSecEscPersona)
                            .then((result) => {
                                let response = result.data;
                                console.log(response);
                                this.$bvToast.toast(
                                    response.msg,
                                    {
                                        title: 'Correcto',
                                        variant: 'success',
                                        autoHideDelay: 5000
                                    }
                                )
      
                                this.getListaPersonasporEscuadras();
                            })
                            .catch(error => {
                                console.log(error);
                            })
                    } else {
                        //swal("Your imaginary file is safe!");
                    }
                });
        },



        buscarEstudiantes(id){
           
            this.$refs['buscar-estudiantes'].show();
            this.ecs = id;
            this.getListaPersonasporEscuadras();
            this.getPersonasporEscuadras(this.ecs);
            this.getEstadoPersona();
            
        },


        verEstudiante(id){
            this.per = id;
            //this.isLoading=true;
            this.ss.showPersona(this.per).then(
                (result) => {
                    let response = result.data;
                    this.persona = response.data;
                    this.$refs['view-estudiante'].show();
                    this.isLoading=false;
                }
            ).catch(error => {
                console.log(error);
                this.isLoading=false;
            });


        },



        

        showEstadoEstudiante(id){

            this.isLoading=true;
            this.ss.showEstadoEstudiante(id).then(
                (result) => {
                    let response = result.data;
                    this.compSecEscPersona = response.data;
                    console.log(this.compSecEscPersona);
                    this.$refs['frm-estadoPersona'].show();
                    this.$refs['view-estudiante'].hide();
                    this.isLoading=false;
                    

                }
            ).catch(error => {
                console.log(error);
                this.isLoading=false;
            });
            
            
        },


        
    saveEstadoEstudiante(){

        this.ss.storeEstadoEstudiante(this.compSecEscPersona).then(
            (result) => {
                console.log(result);
                let response = result.data;
                this.$bvToast.toast(
                    response.msg,
                    {
                        title: 'Correcto',
                        variant: 'success',
                        autoHideDelay: 5000
                    }
                    
                )
                //this.$refs['view-consulta'].show(); //para volver al modal
                console.log(response);
                this.$refs['frm-estadoPersona'].hide();
                this.$refs['buscar-estudiantes'].reload();
            })
            .catch((error) => {
                this.errorBag = error.response.data.errors;
                this.$bvToast.toast(
                    'Problema al Guardar el Registro, favor verificar los Datos',
                    {
                        title: 'Error',
                        variant: 'danger',
                        autoHideDelay: 5000
                    }
                )
                this.getPersonasporEscuadras();
                this.getListaPersonasporEscuadras();
                
            });





    },

        
 

        draw() {
            window.$('.btn-datatable-CompaniaSeccion').on('click', (evt) => {
                const data = window.$(evt.target)[0].id;
                this.showCompaniaSeccion(data);
            });
        }
    },
    components: {
        dataTable,
        Loader
    },
    mounted() {
        var persona = JSON.parse(localStorage.getItem('persona'));
        if (!persona) {
            this.$router.push('/Login');
        } else {
            this.getCompania();
            this.getSeccion();
            this.getEscuadra();
            this.getPersona();
            
        }
    }
};
