import MainService from "@/services/MainService.js";
import dataTable from "@/components/Datatable";
import Loader from "@/components/Loader";
window.$ = window.jQuery = require("jquery");
   
export default {
    name: "TipoAsistenciaPage",
    data() {
        let ss = new MainService();
        return {
            msg: "TipoAsistenciaPage",
            ss: ss,
            ajax: {
                "url": ss.indexTipoAsistencia(),
                headers: ss.getToken(),
            },
            columns: [
                { data: 'id', name: 'id', orderable: false, searchable: false, visible: false },
                { data: 'DT_RowIndex', name: 'DT_RowIndex', title: 'N', orderable: false, searchable: false },
                { data: 'TipoAsistencia', name: 'TipoAsistencia.TipoAsistencia', title: 'Tipo Asistencia' },
                { data: 'Descripcion', name: 'TipoAsistencia.Descripcion', title: 'Descripción' },
                {
                    data: 'action',
                    orderable: false,
                    title: 'Acciones',
                    searchable: false
                },
            ],
            tipoAsistencia: {},
            isLoading: false,
            errorBag: {}
        };
    },
    methods: {
        newTipoAsistencia() {
            this.tipoAsistencia = {};
            this.$refs['frm-tipoAsistencia'].show();
        },
        showTipoAsistencia(id) {
            this.isLoading=true;
            this.ss.showTipoAsistencia(id).then(
                (result) => {
                    let response = result.data;
                    this.tipoAsistencia = response.data;
                    this.$refs['view-tipoAsistencia'].show();
                    this.isLoading=false;
                }
            ).catch(error => {
                console.log(error);
                this.isLoading=false;
            });
        },
        editTipoAsistencia() {
            this.$refs['frm-tipoAsistencia'].show();
            this.$refs['view-tipoAsistencia'].hide();
        },
        cancelTipoAsistencia() {
            if (this.tipoAsistencia.id) {
                this.$refs['view-tipoAsistencia'].show();
            }
            this.$refs['frm-tipoAsistencia'].hide();
        },
        saveTipoAsistencia() {
            this.ss.storeTipoAsistencia(this.tipoAsistencia).then(
                (result) => {
                    console.log(result);
                    let response = result.data;
                    this.$bvToast.toast(
                        response.msg,
                        {
                            title: 'Correcto',
                            variant: 'success',
                            autoHideDelay: 5000
                        }
                    )
                    //this.$refs['view-consulta'].show(); //para volver al modal
                    console.log(response);
                    this.$refs['frm-tipoAsistencia'].hide();
                    this.$refs['datatable-tipoAsistencia'].reload();
                })
                .catch((error) => {
                    this.errorBag = error.response.data.errors;
                    this.$bvToast.toast(
                        'Problema al Guardar el Registro, favor verificar los Datos',
                        {
                            title: 'Error',
                            variant: 'danger',
                            autoHideDelay: 5000
                        }
                    )
                });
        },
        deleteTipoAsistencia() {
            this.$swal({
                title: "Estas seguro que deseas eliminar?",
                text: "Esta accion es irreversible!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {
                        this.ss.destroyTipoAsistencia(this.tipoAsistencia)
                            .then((result) => {
                                let response = result.data;
                                console.log(response);
                                this.$bvToast.toast(
                                    response.msg,
                                    {
                                        title: 'Correcto',
                                        variant: 'success',
                                        autoHideDelay: 5000
                                    }
                                )
                                this.$refs['view-tipoAsistencia'].hide();
                                this.$refs['datatable-tipoAsistencia'].reload();
                            })
                            .catch(error => {
                                console.log(error);
                            })
                    } else {
                        //swal("Your imaginary file is safe!");
                    }
                });
        },
        draw() {
            window.$('.btn-datatable-TipoAsistencia').on('click', (evt) => {
                const data = window.$(evt.target)[0].id;
                this.showTipoAsistencia(data);
            });
        }
    },
    components: {
        dataTable,
        Loader
    },
    mounted() {
        var persona = JSON.parse(localStorage.getItem('persona'));
        if (!persona) {
            this.$router.push('/Login');
        }
    }
};
