import MainService from "@/services/MainService.js";
import dataTable from "@/components/Datatable";
import Loader from "@/components/Loader";
window.$ = window.jQuery = require("jquery");

export default {
    name: "EscuadraPage",
    data() {
        let ss = new MainService();
        return {
            msg: "EscuadraPage",
            ss: ss,
            ajax: {
                "url": ss.indexEscuadra(),
                headers: ss.getToken(),
            },
            columns: [
                { data: 'id', name: 'id', orderable: false, searchable: false, visible: false },
                { data: 'DT_RowIndex', name: 'DT_RowIndex', title: 'N', orderable: false, searchable: false },
                { data: 'NombreEscuadra', name: 'Escuadra.NombreEscuadra', title: 'Nombre Escuadra' },
                { data: 'Descripcion', name: 'Escuadra.Descripcion', title: 'Descripción' },
                {
                    data: 'action',
                    orderable: false,
                    title: 'Acciones',
                    searchable: false
                },
            ],
            //rols: [],
            escuadra: {},
            isLoading: false,
            errorBag: {}
        };
    },
    methods: {
        newEscuadra() {
            this.escuadra = {};
            this.$refs['frm-escuadra'].show();
        },
        showEscuadra(id) {
            this.isLoading=true;
            this.ss.showEscuadra(id).then(
                (result) => {
                    let response = result.data;
                    this.escuadra = response.data;
                    this.$refs['view-escuadra'].show();
                    this.isLoading=false;
                }
            ).catch(error => {
                console.log(error);
                this.isLoading=false;
            });
        },
        editEscuadra() {
            this.$refs['frm-escuadra'].show();
            this.$refs['view-escuadra'].hide();
        },
        cancelEscuadra() {
            if (this.escuadra.id) {
                this.$refs['view-escuadra'].show();
            }
            this.$refs['frm-escuadra'].hide();
        },
        saveEscuadra() {
            this.ss.storeEscuadra(this.escuadra).then(
                (result) => {
                    console.log(result);
                    let response = result.data;
                    this.$bvToast.toast(
                        response.msg,
                        {
                            title: 'Correcto',
                            variant: 'success',
                            autoHideDelay: 5000
                        }
                    )
                    //this.$refs['view-consulta'].show(); //para volver al modal
                    console.log(response);
                    this.$refs['frm-escuadra'].hide();
                    this.$refs['datatable-escuadra'].reload();
                })
                .catch((error) => {
                    this.errorBag = error.response.data.errors;
                    this.$bvToast.toast(
                        'Problema al Guardar el Registro, favor verificar los Datos',
                        {
                            title: 'Error',
                            variant: 'danger',
                            autoHideDelay: 5000
                        }
                    )
                });
        },
        deleteEscuadra() {
            this.$swal({
                title: "Estas seguro que deseas eliminar?",
                text: "Esta accion es irreversible!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {
                        this.ss.destroyEscuadra(this.escuadra)
                            .then((result) => {
                                let response = result.data;
                                console.log(response);
                                this.$bvToast.toast(
                                    response.msg,
                                    {
                                        title: 'Correcto',
                                        variant: 'success',
                                        autoHideDelay: 5000
                                    }
                                )
                                this.$refs['view-escuadra'].hide();
                                this.$refs['datatable-escuadra'].reload();
                            })
                            .catch(error => {
                                console.log(error);
                            })
                    } else {
                        //swal("Your imaginary file is safe!");
                    }
                });
        },
        draw() {
            window.$('.btn-datatable-Escuadra').on('click', (evt) => {
                const data = window.$(evt.target)[0].id;
                this.showEscuadra(data);
            });
        }
    },
    components: {
        dataTable,
        Loader
    },
    mounted() {
        var persona = JSON.parse(localStorage.getItem('persona'));
        if (!persona) {
            this.$router.push('/Login');
        }
    }
};
