import MainService from "@/services/MainService.js";
import dataTable from "@/components/Datatable";
import Loader from "@/components/Loader";
window.$ = window.jQuery = require("jquery");

export default {
    name: "ProfileCeforPage",
    data() {
        let ss = new MainService();
        return {
            msg: "ProfileCeforPage",
            ss: ss,
            ajax: {
                "url": ss.indexCompSecEscPersona(),
                headers: ss.getToken(),
            },
            auth:{},
            persona: {},
            
            unidadAcademicas: [],
            rols: [],
            isLoading: false,
            isLoadingFile:false,
            errorBag: {},
            password: {},
            ruta:0
            
        };
    },
    methods: {

        

        


        getUnidadAcademica() {
            this.ss.listUnidadAcademica().then(
                (result) => {
                    let response = result.data;
                    this.unidadAcademicas = response.data;
                }
            ).catch(error => {
                console.log(error);
            });
        },


        getRol() {
            this.ss.listRol().then(
                (result) => {
                    let response = result.data;
                    this.rols = response.data;
                }
            ).catch(error => {
                console.log(error);
            });
        },


        showInfoCefor() {
            this.isLoading=true;
            this.ss.showInfoCefor(this.auth.id)
                .then(result => {
                    let response = result.data;
                    this.persona = response.data;
                    this.ruta = "http://localhost:8000/storage/documents/documents/" + this.persona.Foto;
                    this.isLoading=false;
                })
                .catch(error => {
                    console.log(error);
                });
        },
        
        
    },
    components: {
        dataTable,
        Loader
    },
    mounted() {
        this.auth = JSON.parse(localStorage.getItem('persona'));
        console.log(this.auth);
        if (!this.auth) {
            this.$router.push('/Login');
        } else {
            //this.showPersona();
            this.showInfoCefor();
            this.getRol();
            this.getUnidadAcademica();
        }
    }
};
