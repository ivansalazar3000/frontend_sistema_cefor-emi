import MainService from "@/services/MainService.js";
import dataTable from "@/components/Datatable";
import Loader from "@/components/Loader";
window.$ = window.jQuery = require("jquery");
   
export default {
    name: "TipoCompaniaPage",
    data() {
        let ss = new MainService();
        return {
            msg: "TipoCompaniaPage",
            ss: ss,
            ajax: {
                "url": ss.indexTipoCompania(),
                headers: ss.getToken(),
            },
            columns: [
                { data: 'id', name: 'id', orderable: false, searchable: false, visible: false },
                { data: 'DT_RowIndex', name: 'DT_RowIndex', title: 'N', orderable: false, searchable: false },
                { data: 'TipoCompania', name: 'TipoCompania.TipoCompania', title: 'Tipo de Compañia' },
                { data: 'TipoCompania', name: 'TipoCompania.Descripcion', title: 'Descripción' },
                {
                    data: 'action',
                    orderable: false,
                    title: 'Acciones',
                    searchable: false
                },
            ],
            tipoCompania: {},
            isLoading: false,
            errorBag: {}
        };
    },
    methods: {
        newTipoCompania() {
            this.tipoCompania = {};
            this.$refs['frm-tipoCompania'].show();
        },
        showTipoCompania(id) {
            this.isLoading=true;
            this.ss.showTipoCompania(id).then(
                (result) => {
                    let response = result.data;
                    this.tipoCompania = response.data;
                    this.$refs['view-tipoCompania'].show();
                    this.isLoading=false;
                }
            ).catch(error => {
                console.log(error);
                this.isLoading=false;
            });
        },
        editTipoCompania() {
            this.$refs['frm-tipoCompania'].show();
            this.$refs['view-tipoCompania'].hide();
        },
        cancelTipoCompania() {
            if (this.tipoCompania.id) {
                this.$refs['view-tipoCompania'].show();
            }
            this.$refs['frm-tipoCompania'].hide();
        },
        saveTipoCompania() {
            this.ss.storeTipoCompania(this.tipoCompania).then(
                (result) => {
                    console.log(result);
                    let response = result.data;
                    this.$bvToast.toast(
                        response.msg,
                        {
                            title: 'Correcto',
                            variant: 'success',
                            autoHideDelay: 5000
                        }
                    )
                    //this.$refs['view-consulta'].show(); //para volver al modal
                    console.log(response);
                    this.$refs['frm-tipoCompania'].hide();
                    this.$refs['datatable-tipoCompania'].reload();
                })
                .catch((error) => {
                    this.errorBag = error.response.data.errors;
                    this.$bvToast.toast(
                        'Problema al Guardar el Registro, favor verificar los Datos',
                        {
                            title: 'Error',
                            variant: 'danger',
                            autoHideDelay: 5000
                        }
                    )
                });
        },
        deleteTipoCompania() {
            this.$swal({
                title: "Estas seguro que deseas eliminar?",
                text: "Esta accion es irreversible!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {
                        this.ss.destroyTipoCompania(this.tipoCompania)
                            .then((result) => {
                                let response = result.data;
                                console.log(response);
                                this.$bvToast.toast(
                                    response.msg,
                                    {
                                        title: 'Correcto',
                                        variant: 'success',
                                        autoHideDelay: 5000
                                    }
                                )
                                this.$refs['view-tipoCompania'].hide();
                                this.$refs['datatable-tipoCompania'].reload();
                            })
                            .catch(error => {
                                console.log(error);
                            })
                    } else {
                        //swal("Your imaginary file is safe!");
                    }
                });
        },
        draw() {
            window.$('.btn-datatable-TipoCompania').on('click', (evt) => {
                const data = window.$(evt.target)[0].id;
                this.showTipoCompania(data);
            });
        }
    },
    components: {
        dataTable,
        Loader
    },
    mounted() {
        var persona = JSON.parse(localStorage.getItem('persona'));
        if (!persona) {
            this.$router.push('/Login');
        }
    }
};
