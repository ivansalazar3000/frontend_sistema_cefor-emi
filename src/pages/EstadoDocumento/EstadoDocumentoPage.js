import MainService from "@/services/MainService.js";
import dataTable from "@/components/Datatable";
import Loader from "@/components/Loader";
window.$ = window.jQuery = require("jquery");
   
export default {
    name: "EstadoDocumentoPage",
    data() {
        let ss = new MainService();
        return {
            msg: "EstadoDocumentoPage",
            ss: ss,
            ajax: {
                "url": ss.indexEstadoDocumento(),
                headers: ss.getToken(),
            },
            columns: [
                { data: 'id', name: 'id', orderable: false, searchable: false, visible: false },
                { data: 'DT_RowIndex', name: 'DT_RowIndex', title: 'N', orderable: false, searchable: false },
                { data: 'EstadoDocumento', name: 'EstadoDocumento.EstadoDocumento', title: 'Estado de Documentos' },
                { data: 'Descripcion', name: 'EstadoDocumento.Descripcion', title: 'Descripción' },
                {
                    data: 'action',
                    orderable: false,
                    title: 'Acciones',
                    searchable: false
                },
            ],
            estadoDocumento: {},
            isLoading: false,
            errorBag: {}
        };
    },
    methods: {
        newEstadoDocumento() {
            this.estadoDocumento = {};
            this.$refs['frm-estadoDocumento'].show();
        },
        showEstadoDocumento(id) {
            this.isLoading=true;
            this.ss.showEstadoDocumento(id).then(
                (result) => {
                    let response = result.data;
                    this.estadoDocumento = response.data;
                    this.$refs['view-estadoDocumento'].show();
                    this.isLoading=false;
                }
            ).catch(error => {
                console.log(error);
                this.isLoading=false;
            });
        },
        editEstadoDocumento() {
            this.$refs['frm-estadoDocumento'].show();
            this.$refs['view-estadoDocumento'].hide();
        },
        cancelEstadoDocumento() {
            if (this.estadoDocumento.id) {
                this.$refs['view-estadoDocumento'].show();
            }
            this.$refs['frm-estadoDocumento'].hide();
        },
        saveEstadoDocumento() {
            this.ss.storeEstadoDocumento(this.estadoDocumento).then(
                (result) => {
                    console.log(result);
                    let response = result.data;
                    this.$bvToast.toast(
                        response.msg,
                        {
                            title: 'Correcto',
                            variant: 'success',
                            autoHideDelay: 5000
                        }
                    )
                    //this.$refs['view-consulta'].show(); //para volver al modal
                    console.log(response);
                    this.$refs['frm-estadoDocumento'].hide();
                    this.$refs['datatable-estadoDocumento'].reload();
                })
                .catch((error) => {
                    this.errorBag = error.response.data.errors;
                    this.$bvToast.toast(
                        'Problema al Guardar el Registro, favor verificar los Datos',
                        {
                            title: 'Error',
                            variant: 'danger',
                            autoHideDelay: 5000
                        }
                    )
                });
        },
        deleteEstadoDocumento() {
            this.$swal({
                title: "Estas seguro que deseas eliminar?",
                text: "Esta accion es irreversible!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {
                        this.ss.destroyEstadoDocumento(this.estadoDocumento)
                            .then((result) => {
                                let response = result.data;
                                console.log(response);
                                this.$bvToast.toast(
                                    response.msg,
                                    {
                                        title: 'Correcto',
                                        variant: 'success',
                                        autoHideDelay: 5000
                                    }
                                )
                                this.$refs['view-estadoDocumento'].hide();
                                this.$refs['datatable-estadoDocumento'].reload();
                            })
                            .catch(error => {
                                console.log(error);
                            })
                    } else {
                        //swal("Your imaginary file is safe!");
                    }
                });
        },
        draw() {
            window.$('.btn-datatable-EstadoDocumento').on('click', (evt) => {
                const data = window.$(evt.target)[0].id;
                this.showEstadoDocumento(data);
            });
        }
    },
    components: {
        dataTable,
        Loader
    },
    mounted() {
        var persona = JSON.parse(localStorage.getItem('persona'));
        if (!persona) {
            this.$router.push('/Login');
        }
    }
};
