import MainService from "@/services/MainService.js";
import dataTable from "@/components/Datatable";
import Loader from "@/components/Loader";
window.$ = window.jQuery = require("jquery");
   
export default {
    name: "EstadoPersonaPage",
    data() {
        let ss = new MainService();
        return {
            msg: "EstadoPersonaPage",
            ss: ss,
            ajax: {
                "url": ss.indexEstadoPersona(),
                headers: ss.getToken(),
            },
            columns: [
                { data: 'id', name: 'id', orderable: false, searchable: false, visible: false },
                { data: 'DT_RowIndex', name: 'DT_RowIndex', title: 'N', orderable: false, searchable: false },
                { data: 'EstadoPersona', name: 'EstadoPersona.EstadoPersona', title: 'Estado' },
                { data: 'Descripcion', name: 'EstadoPersona.Descripcion', title: 'Descripción' },
                {
                    data: 'action',
                    orderable: false,
                    title: 'Acciones',
                    searchable: false
                },
            ],
            estadoPersona: {},
            isLoading: false,
            errorBag: {}
        };
    },
    methods: {
        newEstadoPersona() {
            this.estadoPersona = {};
            this.$refs['frm-estadoPersona'].show();
        },
        showEstadoPersona(id) {
            this.isLoading=true;
            this.ss.showEstadoPersona(id).then(
                (result) => {
                    let response = result.data;
                    this.estadoPersona = response.data;
                    this.$refs['view-estadoPersona'].show();
                    this.isLoading=false;
                }
            ).catch(error => {
                console.log(error);
                this.isLoading=false;
            });
        },
        editEstadoPersona() {
            this.$refs['frm-estadoPersona'].show();
            this.$refs['view-estadoPersona'].hide();
        },
        cancelEstadoPersona() {
            if (this.estadoPersona.id) {
                this.$refs['view-estadoPersona'].show();
            }
            this.$refs['frm-estadoPersona'].hide();
        },
        saveEstadoPersona() {
            this.ss.storeEstadoPersona(this.estadoPersona).then(
                (result) => {
                    console.log(result);
                    let response = result.data;
                    this.$bvToast.toast(
                        response.msg,
                        {
                            title: 'Correcto',
                            variant: 'success',
                            autoHideDelay: 5000
                        }
                    )
                    //this.$refs['view-consulta'].show(); //para volver al modal
                    console.log(response);
                    this.$refs['frm-estadoPersona'].hide();
                    this.$refs['datatable-estadoPersona'].reload();
                })
                .catch((error) => {
                    this.errorBag = error.response.data.errors;
                    this.$bvToast.toast(
                        'Problema al Guardar el Registro, favor verificar los Datos',
                        {
                            title: 'Error',
                            variant: 'danger',
                            autoHideDelay: 5000
                        }
                    )
                });
        },
        deleteEstadoPersona() {
            this.$swal({
                title: "Estas seguro que deseas eliminar?",
                text: "Esta accion es irreversible!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {
                        this.ss.destroyEstadoPersona(this.estadoPersona)
                            .then((result) => {
                                let response = result.data;
                                console.log(response);
                                this.$bvToast.toast(
                                    response.msg,
                                    {
                                        title: 'Correcto',
                                        variant: 'success',
                                        autoHideDelay: 5000
                                    }
                                )
                                this.$refs['view-estadoPersona'].hide();
                                this.$refs['datatable-estadoPersona'].reload();
                            })
                            .catch(error => {
                                console.log(error);
                            })
                    } else {
                        //swal("Your imaginary file is safe!");
                    }
                });
        },
        draw() {
            window.$('.btn-datatable-EstadoPersona').on('click', (evt) => {
                const data = window.$(evt.target)[0].id;
                this.showEstadoPersona(data);
            });
        }
    },
    components: {
        dataTable,
        Loader
    },
    mounted() {
        var persona = JSON.parse(localStorage.getItem('persona'));
        if (!persona) {
            this.$router.push('/Login');
        }
    }
};
