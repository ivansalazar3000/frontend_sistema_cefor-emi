import MainService from "@/services/MainService.js";
import dataTable from "@/components/Datatable";
import Loader from "@/components/Loader";
window.$ = window.jQuery = require("jquery");

export default {
    name: "SeccionPage",
    data() {
        let ss = new MainService();
        return {
            msg: "SeccionPage",
            ss: ss,
            ajax: {
                "url": ss.indexSeccion(),
                headers: ss.getToken(),
            },
            columns: [
                { data: 'id', name: 'id', orderable: false, searchable: false, visible: false },
                { data: 'DT_RowIndex', name: 'DT_RowIndex', title: 'N', orderable: false, searchable: false },
                { data: 'NombreSeccion', name: 'Seccion.NombreSeccion', title: 'Nombre Sección' },
                { data: 'Descripcion', name: 'Seccion.Descripcion', title: 'Descripción' },
                {
                    data: 'action',
                    orderable: false,
                    title: 'Acciones',
                    searchable: false
                },
            ],
            //rols: [],
            seccion: {},
            isLoading: false,
            errorBag: {}
        };
    },
    methods: {
        newSeccion() {
            this.seccion = {};
            this.$refs['frm-seccion'].show();
        },
        showSeccion(id) {
            this.isLoading=true;
            this.ss.showSeccion(id).then(
                (result) => {
                    let response = result.data;
                    this.seccion = response.data;
                    this.$refs['view-seccion'].show();
                    this.isLoading=false;
                }
            ).catch(error => {
                console.log(error);
                this.isLoading=false;
            });
        },
        editSeccion() {
            this.$refs['frm-seccion'].show();
            this.$refs['view-seccion'].hide();
        },
        cancelSeccion() {
            if (this.seccion.id) {
                this.$refs['view-seccion'].show();
            }
            this.$refs['frm-seccion'].hide();
        },
        saveSeccion() {
            this.ss.storeSeccion(this.seccion).then(
                (result) => {
                    console.log(result);
                    let response = result.data;
                    this.$bvToast.toast(
                        response.msg,
                        {
                            title: 'Correcto',
                            variant: 'success',
                            autoHideDelay: 5000
                        }
                    )
                    //this.$refs['view-consulta'].show(); //para volver al modal
                    console.log(response);
                    this.$refs['frm-seccion'].hide();
                    this.$refs['datatable-seccion'].reload();
                })
                .catch((error) => {
                    this.errorBag = error.response.data.errors;
                    this.$bvToast.toast(
                        'Problema al Guardar el Registro, favor verificar los Datos',
                        {
                            title: 'Error',
                            variant: 'danger',
                            autoHideDelay: 5000
                        }
                    )
                    
                });
        },
        deleteSeccion() {
            this.$swal({
                title: "Estas seguro que deseas eliminar?",
                text: "Esta accion es irreversible!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {
                        this.ss.destroySeccion(this.seccion)
                            .then((result) => {
                                let response = result.data;
                                console.log(response);
                                this.$bvToast.toast(
                                    response.msg,
                                    {
                                        title: 'Correcto',
                                        variant: 'success',
                                        autoHideDelay: 5000
                                    }
                                )
                                this.$refs['view-seccion'].hide();
                                this.$refs['datatable-seccion'].reload();
                            })
                            .catch(error => {
                                console.log(error);
                            })
                    } else {
                        //swal("Your imaginary file is safe!");
                    }
                });
        },
        draw() {
            window.$('.btn-datatable-Seccion').on('click', (evt) => {
                const data = window.$(evt.target)[0].id;
                this.showSeccion(data);
            });
        }
    },
    components: {
        dataTable,
        Loader
    },
    mounted() {
        var persona = JSON.parse(localStorage.getItem('persona'));
        if (!persona) {
            this.$router.push('/Login');
        }
    }
};
