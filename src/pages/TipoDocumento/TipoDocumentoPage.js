import MainService from "@/services/MainService.js";
import dataTable from "@/components/Datatable";
import Loader from "@/components/Loader";
window.$ = window.jQuery = require("jquery");
   
export default {
    name: "TipoDocumentoPage",
    data() {
        let ss = new MainService();
        return {
            msg: "TipoDocumentoPage",
            ss: ss,
            ajax: {
                "url": ss.indexTipoDocumento(),
                headers: ss.getToken(),
            },
            columns: [
                { data: 'id', name: 'id', orderable: false, searchable: false, visible: false },
                { data: 'DT_RowIndex', name: 'DT_RowIndex', title: 'N', orderable: false, searchable: false },
                { data: 'TipoDocumento', name: 'TipoDocumento.TipoDocumento', title: 'Tipo de Documento' },
                { data: 'Descripcion', name: 'TipoDocumento.Descripcion', title: 'Descripción' },
                {
                    data: 'action',
                    orderable: false,
                    title: 'Acciones',
                    searchable: false
                },
            ],
            tipoDocumento: {},
            isLoading: false,
            errorBag: {}
        };
    },
    methods: {
        newTipoDocumento() {
            this.tipoDocumento = {};
            this.$refs['frm-tipoDocumento'].show();
        },
        showTipoDocumento(id) {
            this.isLoading=true;
            this.ss.showTipoDocumento(id).then(
                (result) => {
                    let response = result.data;
                    this.tipoDocumento = response.data;
                    this.$refs['view-tipoDocumento'].show();
                    this.isLoading=false;
                }
            ).catch(error => {
                console.log(error);
                this.isLoading=false;
            });
        },
        editTipoDocumento() {
            this.$refs['frm-tipoDocumento'].show();
            this.$refs['view-tipoDocumento'].hide();
        },
        cancelTipoDocumento() {
            if (this.tipoDocumento.id) {
                this.$refs['view-tipoDocumento'].show();
            }
            this.$refs['frm-tipoDocumento'].hide();
        },
        saveTipoDocumento() {
            this.ss.storeTipoDocumento(this.tipoDocumento).then(
                (result) => {
                    console.log(result);
                    let response = result.data;
                    this.$bvToast.toast(
                        response.msg,
                        {
                            title: 'Correcto',
                            variant: 'success',
                            autoHideDelay: 5000
                        }
                    )
                    //this.$refs['view-consulta'].show(); //para volver al modal
                    console.log(response);
                    this.$refs['frm-tipoDocumento'].hide();
                    this.$refs['datatable-tipoDocumento'].reload();
                })
                .catch((error) => {
                    this.errorBag = error.response.data.errors;
                    this.$bvToast.toast(
                        'Problema al Guardar el Registro, favor verificar los Datos',
                        {
                            title: 'Error',
                            variant: 'danger',
                            autoHideDelay: 5000
                        }
                    )
                });
        },
        deleteTipoDocumento() {
            this.$swal({
                title: "Estas seguro que deseas eliminar?",
                text: "Esta accion es irreversible!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {
                        this.ss.destroyTipoDocumento(this.tipoDocumento)
                            .then((result) => {
                                let response = result.data;
                                console.log(response);
                                this.$bvToast.toast(
                                    response.msg,
                                    {
                                        title: 'Correcto',
                                        variant: 'success',
                                        autoHideDelay: 5000
                                    }
                                )
                                this.$refs['view-tipoDocumento'].hide();
                                this.$refs['datatable-tipoDocumento'].reload();
                            })
                            .catch(error => {
                                console.log(error);
                            })
                    } else {
                        //swal("Your imaginary file is safe!");
                    }
                });
        },
        draw() {
            window.$('.btn-datatable-TipoDocumento').on('click', (evt) => {
                const data = window.$(evt.target)[0].id;
                this.showTipoDocumento(data);
            });
        }
    },
    components: {
        dataTable,
        Loader
    },
    mounted() {
        var persona = JSON.parse(localStorage.getItem('persona'));
        if (!persona) {
            this.$router.push('/Login');
        }
    }
};
