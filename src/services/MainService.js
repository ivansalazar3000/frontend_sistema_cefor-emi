var axios = require('axios');

export default class MainService {

    constructor() {
        var token = JSON.parse(localStorage.getItem('token'));
        axios.defaults.baseURL = process.env.VUE_APP_MAIN_SERVICE;
        if (token) {
            axios.defaults.headers.common = {
                'Authorization': 'Bearer ' + token,
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            }
        }else{
            axios.defaults.headers.common = {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            }
        }
    }

    getToken() {
        return axios.defaults.headers.common;
    }

    login(item) {
        return axios.post('api/login', item);
    }

    uploadFile(item) {
        return axios.post('api/uploadFile', item);
    }

    loginToken365(item) {
        return axios.post('api/loginToken365', item);
    }


    // persona
    indexPersona() {
        return axios.defaults.baseURL + 'api/Persona/index';
    }

    showPersona(id) {
        return axios.get('api/Persona/show?id=' + id);
    }

    listPersona() {
        return axios.get('api/Persona/list');
    }

    listPersonaColaborador() {
        return axios.get('api/Persona/listPersonaColaborador');
    }

    storePersona(item) {
        return axios.post('api/Persona/store', item);
    }

    destroyPersona(item) {
        return axios.post('api/Persona/destroy', item);
    }

    changePassword(item) {
        return axios.post('api/Persona/changePassword', item);
    }


      // filiacion persona

    generarFiliacion(id) {
        return axios.get('api/Filiacion/generar?id=' + id);
    }






    // unidad academica
    showUnidadAcademica(id) {
        return axios.get('api/UnidadAcademica/show?id=' + id);
    }

    indexUnidadAcademica() {
        return axios.defaults.baseURL + 'api/UnidadAcademica/index';
    }

    listUnidadAcademica() {
        return axios.get('api/UnidadAcademica/list');
    }

    storeUnidadAcademica(item) {
        return axios.post('api/UnidadAcademica/store', item);
    }

    destroyUnidadAcademica(item) {
        return axios.post('api/UnidadAcademica/destroy', item);
    }


    // rol
    showRol(id) {
        return axios.get('api/Rol/show?id=' + id);
    }

    indexRol() {
        return axios.defaults.baseURL + 'api/Rol/index';
    }

    listRol() {
        return axios.get('api/Rol/list');
    }

    storeRol(item) {
        return axios.post('api/Rol/store', item);
    }

    destroyRol(item) {
        return axios.post('api/Rol/destroy', item);
    }



    //TipoReporte
    showTipoReporte(id) {
        return axios.get('api/TipoReporte/show?id=' + id);
    }

    indexTipoReporte() {
        return axios.defaults.baseURL + 'api/TipoReporte/index';
    }

    listTipoReporte() {
        return axios.get('api/TipoReporte/list');
    }

    storeTipoReporte(item) {
        return axios.post('api/TipoReporte/store', item);
    }

    destroyTipoReporte(item) {
        return axios.post('api/TipoReporte/destroy', item);
    }

    generateTipoReporte(item) {
        return axios.post('api/TipoReporte/generate', item);
    }



    
    // tipo compania
    showTipoCompania(id) {
        return axios.get('api/TipoCompania/show?id=' + id);
    }

    indexTipoCompania() {
        return axios.defaults.baseURL + 'api/TipoCompania/index';
    }

    listTipoCompania() {
        return axios.get('api/TipoCompania/list');
    }

    storeTipoCompania(item) {
        return axios.post('api/TipoCompania/store', item);
    }

    destroyTipoCompania(item) {
        return axios.post('api/TipoCompania/destroy', item);
    }


    // tipo documento 

    showTipoDocumento(id) {
        return axios.get('api/TipoDocumento/show?id=' + id);
    }

    indexTipoDocumento() {
        return axios.defaults.baseURL + 'api/TipoDocumento/index';
    }

    listTipoDocumento() {
        return axios.get('api/TipoDocumento/list');
    }

    storeTipoDocumento(item) {
        return axios.post('api/TipoDocumento/store', item);
    }

    destroyTipoDocumento(item) {
        return axios.post('api/TipoDocumento/destroy', item);
    }

    // tipo asistencia 

    showTipoAsistencia(id) {
        return axios.get('api/TipoAsistencia/show?id=' + id);
    }

    indexTipoAsistencia() {
        return axios.defaults.baseURL + 'api/TipoAsistencia/index';
    }

    listTipoAsistencia() {
        return axios.get('api/TipoAsistencia/list');
    }

    storeTipoAsistencia(item) {
        return axios.post('api/TipoAsistencia/store', item);
    }

    destroyTipoAsistencia(item) {
        return axios.post('api/TipoAsistencia/destroy', item);
    }


    // escuadra

    showEscuadra(id) {
        return axios.get('api/Escuadra/show?id=' + id);
    }

    indexEscuadra() {
        return axios.defaults.baseURL + 'api/Escuadra/index';
    }

    listEscuadra() {
        return axios.get('api/Escuadra/list');
    }

    storeEscuadra(item) {
        return axios.post('api/Escuadra/store', item);
    }

    destroyEscuadra(item) {
        return axios.post('api/Escuadra/destroy', item);
    }

    //seccion
    
    showSeccion(id) {
        return axios.get('api/Seccion/show?id=' + id);
    }

    indexSeccion() {
        return axios.defaults.baseURL + 'api/Seccion/index';
    }

    listSeccion() {
        return axios.get('api/Seccion/list');
    }

    storeSeccion(item) {
        return axios.post('api/Seccion/store', item);
    }

    destroySeccion(item) {
        return axios.post('api/Seccion/destroy', item);
    }


    //compania

    showCompania(id) {
        return axios.get('api/Compania/show?id=' + id);
    }

    indexCompania() {
        return axios.defaults.baseURL + 'api/Compania/index';
    }

    listCompania() {
        return axios.get('api/Compania/list');
    }

    storeCompania(item) {
        return axios.post('api/Compania/store', item);
    }

    destroyCompania(item) {
        return axios.post('api/Compania/destroy', item);
    }


    // estado asistencia
    showEstadoAsistencia(id) {
        return axios.get('api/EstadoAsistencia/show?id=' + id);
    }

    indexEstadoAsistencia() {
        return axios.defaults.baseURL + 'api/EstadoAsistencia/index';
    }

    listEstadoAsistencia() {
        return axios.get('api/EstadoAsistencia/list');
    }

    storeEstadoAsistencia(item) {
        return axios.post('api/EstadoAsistencia/store', item);
    }

    destroyEstadoAsistencia(item) {
        return axios.post('api/EstadoAsistencia/destroy', item);
    }


    // estado persona
    showEstadoPersona(id) {
        return axios.get('api/EstadoPersona/show?id=' + id);
    }

    indexEstadoPersona() {
        return axios.defaults.baseURL + 'api/EstadoPersona/index';
    }

    listEstadoPersona() {
        return axios.get('api/EstadoPersona/list');
    }

    storeEstadoPersona(item) {
        return axios.post('api/EstadoPersona/store', item);
    }

    destroyEstadoPersona(item) {
        return axios.post('api/EstadoPersona/destroy', item);
    }


    // estado documento
    showEstadoDocumento(id) {
        return axios.get('api/EstadoDocumento/show?id=' + id);
    }

    indexEstadoDocumento() {
        return axios.defaults.baseURL + 'api/EstadoDocumento/index';
    }

    listEstadoDocumento() {
        return axios.get('api/EstadoDocumento/list');
    }

    storeEstadoDocumento(item) {
        return axios.post('api/EstadoDocumento/store', item);
    }

    destroyEstadoDocumento(item) {
        return axios.post('api/EstadoDocumento/destroy', item);
    }


    //companiaSeccion

    showCompaniaSeccion(id) {
        return axios.get('api/CompaniaSeccion/show?id=' + id);
    }

    indexCompaniaSeccion() {
        return axios.defaults.baseURL + 'api/CompaniaSeccion/index';
    }

    listCompaniaSeccion() {
        return axios.get('api/CompaniaSeccion/list'); 
    }

    storeCompaniaSeccion(item) {
        return axios.post('api/CompaniaSeccion/store', item);
    }

    destroyCompaniaSeccion(item) {
        return axios.post('api/CompaniaSeccion/destroy', item);
    }

    


    //companiaSeccionEscuadra


    showCompaniaSeccionEscuadra(id) {
        return axios.get('api/CompaniaSeccionEscuadra/show?id=' + id);
    }

    indexCompaniaSeccionEscuadra() {
        return axios.defaults.baseURL + 'api/CompaniaSeccionEscuadra/index';
    }

    storeCompaniaSeccionEscuadra(item) {
        return axios.post('api/CompaniaSeccionEscuadra/store', item);
    }

    listCompaniaSeccionEscuadra(id) {
        return axios.get('api/CompaniaSeccionEscuadra/list?CompaniaSeccion=' + id);
    }

    destroyCompaniaSeccionEscuadra(item) {
        return axios.post('api/CompaniaSeccionEscuadra/destroy', item);
    }


    //companiaSeccionEscuadraPersona

    showCompSecEscPersona(id) {
        return axios.get('api/CompSecEscPersona/show?id=' + id);
    }

    showInfoCefor(id) {
        return axios.get('api/CompSecEscPersona/showInfoCefor?id=' + id);
    }

    indexCompSecEscPersona() {
        return axios.defaults.baseURL + 'api/CompSecEscPersona/index';
    }

    listCompSecEscPersona(id) {
        return axios.get('api/CompSecEscPersona/list?CompaniaSeccionEscuadra=' + id); 
    }

    storeCompSecEscPersona(item) {
        return axios.post('api/CompSecEscPersona/store', item);
    }

    destroyCompSecEscPersona(item) {
        return axios.post('api/CompSecEscPersona/destroy', item);
    }

    showEstadoEstudiante(id) {
        return axios.get('api/CompSecEscPersona/showEstado?id=' + id);
    }

    
    storeEstadoEstudiante(item) {
        return axios.post('api/CompSecEscPersona/storeEstado', item);
    }


    // Documentacion
    showDocumento(id) {
        return axios.get('api/Documentacion/show?id=' + id);
    }

    indexDocumento() {
        return axios.defaults.baseURL + 'api/Documentacion/index';
    }

    listDocumentosporPersona(id) {
        return axios.get('api/Documentacion/listDocumentosporPersona?id=' + id);
    }

    storeDocumento(item) {
        return axios.post('api/Documentacion/store', item);
    }

    destroyDocumento(item) {
        return axios.post('api/Documentacion/destroy', item);
    }

    revisionDocumento(item) {
        return axios.post('api/Documentacion/revision', item);
    }

    // asistencia
    showAsistencia(id) {
        return axios.get('api/Asistencia/show?id=' + id);
    }

    indexAsistencia() {
        return axios.defaults.baseURL + 'api/Asistencia/index';
    }

    listAsistencia() {
        return axios.get('api/Asistencia/list');
    }

    storeAsistencia(item) {
        return axios.post('api/Asistencia/store', item);
    }

    storeRevista(item) {
        return axios.post('api/Asistencia/storeRevista', item);
    }

    destroyAsistencia(item) {
        return axios.post('api/Asistencia/destroy', item);
    }

     // certificacion

     indexCertificado() {
        return axios.defaults.baseURL + 'api/Certificado/index';
    }
    // certificacion persona

    generarCertificado(id) {
        return axios.get('api/Certificado/generar?id=' + id);
    }

}